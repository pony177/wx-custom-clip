# coding=UTF-8
import base64
import cv2
import numpy as np
from flask import Flask, request

def max_filtering(N, I_temp):
  wall = np.full((I_temp.shape[0]+(N//2)*2, I_temp.shape[1]+(N//2)*2), -1)
  wall[(N//2):wall.shape[0]-(N//2), (N//2):wall.shape[1]-(N//2)] = I_temp.copy()
  temp = np.full((I_temp.shape[0]+(N//2)*2, I_temp.shape[1]+(N//2)*2), -1)
  for y in range(0,wall.shape[0]):
    for x in range(0,wall.shape[1]):
      if wall[y,x]!=-1:
        window = wall[y-(N//2):y+(N//2)+1,x-(N//2):x+(N//2)+1]
        num = np.amax(window)
        temp[y,x] = num
  A = temp[(N//2):wall.shape[0]-(N//2), (N//2):wall.shape[1]-(N//2)].copy()
  return A

def min_filtering(N, A):
  wall_min = np.full((A.shape[0]+(N//2)*2, A.shape[1]+(N//2)*2), 300)
  wall_min[(N//2):wall_min.shape[0]-(N//2), (N//2):wall_min.shape[1]-(N//2)] = A.copy()
  temp_min = np.full((A.shape[0]+(N//2)*2, A.shape[1]+(N//2)*2), 300)
  for y in range(0,wall_min.shape[0]):
    for x in range(0,wall_min.shape[1]):
      if wall_min[y,x]!=300:
        window_min = wall_min[y-(N//2):y+(N//2)+1,x-(N//2):x+(N//2)+1]
        num_min = np.amin(window_min)
        temp_min[y,x] = num_min
  B = temp_min[(N//2):wall_min.shape[0]-(N//2), (N//2):wall_min.shape[1]-(N//2)].copy()
  return B

def background_subtraction(I, B):
  O = I - B
  norm_img = cv2.normalize(O, None, 0,255, norm_type=cv2.NORM_MINMAX)
  return norm_img

def min_max_filtering(M, N, I):
  if M == 0:
    #max_filtering
    A = max_filtering(N, I)
    #min_filtering
    B = min_filtering(N, A)
    #subtraction
    normalised_img = background_subtraction(I, B)
  elif M == 1:
    #min_filtering
    A = min_filtering(N, I)
    #max_filtering
    B = max_filtering(N, A)
    #subtraction
    normalised_img = background_subtraction(I, B)
  return normalised_img

def correctImage(
  img,
  pointer,
  size = [400, 800],
  shadow = 0
):
  mSize = size
  pts_o = np.float32(pointer)
  pts_d = np.float32([[0, 0], [mSize[0], 0], [0, mSize[1]], [mSize[0], mSize[1]]])
  M = cv2.getPerspectiveTransform(pts_o, pts_d)
  dst = cv2.warpPerspective(img, M, (mSize[0], mSize[1]))
  if int(shadow) == 1:
    dst = min_max_filtering(M = 0, N = 20, I = cv2.cvtColor(dst,cv2.COLOR_RGB2GRAY))
  mBase64 = base64.b64encode(cv2.imencode('.jpg', dst)[1].tostring())
  return 'data:image/jpg;base64,' + mBase64

app = Flask(__name__, static_folder='./static')

@app.route('/', methods = ['post'])
def uploadImage():
  mImage = request.files['file'].read()
  mPointer = request.form['pointer']
  mSize = request.form['size']
  mShadow = request.form['shadow']
  mImage = correctImage(
    cv2.imdecode(np.frombuffer(mImage, np.uint8), cv2.IMREAD_COLOR),
    eval(mPointer),
    eval(mSize),
    mShadow
  )
  return mImage

if __name__ == '__main__':
  app.run(
    host = '0.0.0.0',
    port = 8097,
    debug = False
  )
