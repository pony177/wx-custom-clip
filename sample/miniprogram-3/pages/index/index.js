Page({
  data: {
    show: true,
    src: '/static/754778.jpg',
    image: '',
    style: {
      lineColor: '#4b5cc4',
      dotColor: '#ccc'
    },
    dot: []
  },

  onCancel() {
    this.setData({ show: false })
  },

  async onChoose() {
    const { tempFiles :[{ tempFilePath }] } = await wx.chooseMedia({ mediaType: ['image'], count: 1 })
    this.setData({
      show: true,
      src: tempFilePath
    })
  },

  onConfirm({ detail: { dot, url } }) {
    console.log('四个点：' + JSON.stringify(dot))
    console.log('url：' + url)
    this.setData({
      dot,
      show: false,
      image: url
    })
  }
})