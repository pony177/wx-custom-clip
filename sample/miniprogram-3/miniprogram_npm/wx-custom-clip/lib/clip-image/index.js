"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ImageControl_1 = __importDefault(require("./utils/ImageControl"));
const TOUCH_LIMIT = 8;
const DOT_SIZE = 15;
const PADDING_SIZE = {
    top: 30,
    left: 30
};
Component({
    properties: {
        src: String,
        style: {
            type: Object,
            value: {
                lineColor: '#FFCA28',
                dotColor: '#FD3960',
                shadowColor: '#FFCA28'
            }
        }
    },
    data: {
        dot: [
            { name: '左上', x: 10, y: 10 },
            { name: '右上', x: 100, y: 10 },
            { name: '左下', x: 10, y: 100 },
            { name: '右下', x: 100, y: 100 },
        ],
        ctx: {},
        canvas: {},
        image: {},
        screenSize: {},
        imageSize: {},
    },
    methods: {
        async onSubmit() {
            const { image, imageSize, dot, screenSize } = this.data;
            const mHeightLongRate = image.height / imageSize.height;
            const mWidthLongRate = image.width / (imageSize.width - DOT_SIZE);
            const mImagePointer = this.getImagePointer(imageSize, screenSize);
            let mDot = dot.map(item => {
                return {
                    x: (item.x - mImagePointer.x) * mWidthLongRate,
                    y: (item.y - mImagePointer.y) * mHeightLongRate
                };
            });
            const canvas = wx.createOffscreenCanvas({
                type: '2d',
                width: image.width,
                height: image.height
            });
            const ctx = canvas.getContext('2d');
            ctx.moveTo(mDot[0].x, mDot[0].y);
            ctx.lineTo(mDot[1].x, mDot[1].y);
            ctx.lineTo(mDot[3].x, mDot[3].y);
            ctx.lineTo(mDot[2].x, mDot[2].y);
            ctx.lineTo(mDot[0].x, mDot[0].y);
            ctx.clip();
            ctx.drawImage(image, 0, 0, image.width, image.height);
            const mImageControl = new ImageControl_1.default({
                canvas,
                ctx,
                image: ctx.getImageData(0, 0, image.width, image.height)
            });
            const mBase64 = mImageControl.clip().ctx.canvas.toDataURL();
            const fs = wx.getFileSystemManager();
            const mFilePath = `${wx.env.USER_DATA_PATH}/${new Date().getTime()}.jpg`;
            fs.writeFileSync(mFilePath, mBase64.slice(22), 'base64');
            this.triggerEvent('confirm', {
                dot: mDot,
                url: mFilePath
            });
        },
        drawRoundRectPath(options) {
            const { ctx, width, height, radius } = options;
            ctx.beginPath();
            ctx.arc(width - radius, height - radius, radius, 0, Math.PI / 2);
            ctx.lineTo(radius, height);
            ctx.arc(radius, height - radius, radius, Math.PI / 2, Math.PI);
            ctx.lineTo(0, radius);
            ctx.arc(radius, radius, radius, Math.PI, Math.PI * 3 / 2);
            ctx.lineTo(width - radius, 0);
            ctx.arc(width - radius, radius, radius, Math.PI * 3 / 2, Math.PI * 2);
            ctx.lineTo(width, height - radius);
            ctx.clip();
            ctx.closePath();
        },
        onCancel() {
            this.triggerEvent('cancel');
        },
        drawSpace() {
            const { image, imageSize, canvas, ctx, style, screenSize } = this.data;
            const mDot = this.data.dot;
            canvas.height = screenSize.height;
            canvas.width = screenSize.width;
            const mImagePointer = this.getImagePointer(imageSize, screenSize);
            ctx.drawImage(image, mImagePointer.x + DOT_SIZE / 2, mImagePointer.y + DOT_SIZE / 2, imageSize.width - DOT_SIZE, imageSize.height);
            ctx.moveTo(mDot[0].x + TOUCH_LIMIT, mDot[0].y + TOUCH_LIMIT);
            ctx.lineTo(mDot[1].x + TOUCH_LIMIT, mDot[1].y + TOUCH_LIMIT);
            ctx.lineTo(mDot[3].x + TOUCH_LIMIT, mDot[3].y + TOUCH_LIMIT);
            ctx.lineTo(mDot[2].x + TOUCH_LIMIT, mDot[2].y + TOUCH_LIMIT);
            ctx.lineTo(mDot[0].x + TOUCH_LIMIT, mDot[0].y + TOUCH_LIMIT);
            ctx.lineWidth = 2;
            ctx.strokeStyle = style.lineColor || '#FFCA28';
            ctx.stroke();
            ctx.fillStyle = style.dotColor || '#FD3960';
            ctx.shadowOffsetX = 2;
            ctx.shadowOffsetY = 2;
            ctx.shadowBlur = 8;
            ctx.shadowColor = style.shadowColor || '#FFCA28';
            ctx.globalAlpha = 0.8;
            ctx.fillRect(mDot[0].x, mDot[0].y, DOT_SIZE, DOT_SIZE);
            ctx.fillRect(mDot[1].x, mDot[1].y, DOT_SIZE, DOT_SIZE);
            ctx.fillRect(mDot[2].x, mDot[2].y, DOT_SIZE, DOT_SIZE);
            ctx.fillRect(mDot[3].x, mDot[3].y, DOT_SIZE, DOT_SIZE);
        },
        touchPointToDotIndex(x, y) {
            const mTouchX = x;
            const mTouchY = y;
            const mDot = this.data.dot.map(dot => ({ name: dot.name, x: dot.x + TOUCH_LIMIT, y: dot.y + TOUCH_LIMIT }));
            for (const i in mDot) {
                if (Math.abs(mTouchX - mDot[i].x) <= 30 && Math.abs(mTouchY - mDot[i].y) <= 30) {
                    return ~~i;
                }
            }
            return null;
        },
        hitBorder(x, y) {
            const { screenSize } = this.data;
            let mX = x, mY = y;
            if (x <= 0) {
                mX = 0;
            }
            else if (x >= screenSize.width - DOT_SIZE) {
                mX = screenSize.width - DOT_SIZE;
            }
            if (y <= 0) {
                mY = 0;
            }
            else if (y >= screenSize.height - DOT_SIZE) {
                mY = screenSize.height - DOT_SIZE;
            }
            return { x: mX, y: mY };
        },
        onCanvasTouchMove(event) {
            if (event.touches && event.touches.length > 0) {
                const [{ x: mTouchX, y: mTouchY }] = event.touches;
                const mPointIndex = this.touchPointToDotIndex(mTouchX, mTouchY);
                if (mPointIndex !== null) {
                    const mTouchDot = this.data.dot[mPointIndex];
                    if (mTouchDot) {
                        this.data.dot[mPointIndex] = {
                            ...mTouchDot,
                            ...this.hitBorder(mTouchX, mTouchY)
                        };
                        this.drawSpace();
                    }
                }
            }
        },
        getImagePointer(imageSize, screenSize) {
            return {
                x: screenSize.width / 2 - imageSize.width / 2,
                y: screenSize.height / 2 - imageSize.height / 2
            };
        }
    },
    lifetimes: {
        attached() {
            const mQuery = this.createSelectorQuery();
            const mDom = mQuery.select('#canvas');
            mDom.node().exec(async (res) => {
                if (res && res[0]) {
                    const [{ node: canvas }] = res;
                    const ctx = canvas.getContext('2d');
                    this.data.ctx = ctx;
                    this.data.canvas = canvas;
                    const mImage = canvas.createImage();
                    mImage.src = this.data.src;
                    mImage.onload = () => {
                        const mSystemInfo = wx.getSystemInfoSync();
                        const mImageSize = {
                            width: mImage.width,
                            height: mImage.height,
                            scaleRate: mImage.height / mImage.width
                        };
                        const mScreenSize = {
                            width: mSystemInfo.windowWidth,
                            height: mSystemInfo.windowHeight - 100,
                            scaleRate: (mSystemInfo.windowHeight - 100) / (mSystemInfo.windowWidth)
                        };
                        if (mImageSize.scaleRate < mScreenSize.scaleRate) {
                            mImageSize.width = mScreenSize.width - PADDING_SIZE.left;
                            mImageSize.height = mImageSize.width * mImageSize.scaleRate;
                            if (mImageSize.height > mScreenSize.height) {
                                mImageSize.height = mScreenSize.height - PADDING_SIZE.top;
                            }
                        }
                        else {
                            mImageSize.height = mScreenSize.height - PADDING_SIZE.top;
                            mImageSize.width = mImageSize.height * mImageSize.scaleRate;
                            if (mImageSize.width > mScreenSize.width) {
                                mImageSize.width = mScreenSize.width - PADDING_SIZE.left;
                            }
                        }
                        const mImagePointer = this.getImagePointer(mImageSize, mScreenSize);
                        this.setData({
                            imageSize: mImageSize,
                            screenSize: mScreenSize,
                            dot: [
                                { name: '左上', x: mImagePointer.x, y: mImagePointer.y },
                                { name: '右上', x: mImageSize.width, y: mImagePointer.y },
                                { name: '左下', x: mImagePointer.x, y: mImagePointer.y + mImageSize.height },
                                { name: '右下', x: mImageSize.width, y: mImagePointer.y + mImageSize.height },
                            ],
                            image: mImage
                        }, () => this.drawSpace());
                    };
                }
            });
        }
    }
});
