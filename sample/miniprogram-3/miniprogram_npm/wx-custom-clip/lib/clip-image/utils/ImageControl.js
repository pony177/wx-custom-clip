"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class ImageControl {
    constructor(options) {
        const { ctx, canvas, image } = options;
        this.canvas = canvas;
        this.ctx = ctx;
        this.image = image;
    }
    radius(size) {
        const { ctx, image } = this;
        const { width, height } = image;
        ctx.beginPath();
        ctx.arc(width - size, height - size, size, 0, Math.PI / 2);
        ctx.lineTo(size, height);
        ctx.arc(size, height - size, size, Math.PI / 2, Math.PI);
        ctx.lineTo(0, size);
        ctx.arc(size, size, size, Math.PI, Math.PI * 3 / 2);
        ctx.lineTo(width - size, 0);
        ctx.arc(width - size, size, size, Math.PI * 3 / 2, Math.PI * 2);
        ctx.lineTo(width, height - size);
        ctx.clip();
        ctx.closePath();
        return this;
    }
    clip() {
        const { canvas: mCanvas, ctx, image: mImage } = this;
        const mImageData = mImage;
        const mImageDataArray = mImageData.data;
        const mBound = {
            top: null,
            left: null,
            right: null,
            bottom: null
        };
        let x, y;
        for (let i = 0; i < mImageDataArray.length; i += 4) {
            if (mImageDataArray[i + 3] !== 0) {
                x = (i / 4) % mCanvas.width;
                y = ~~((i / 4) / mCanvas.width);
                if (mBound.top === null) {
                    mBound.top = y;
                }
                if (mBound.left === null) {
                    mBound.left = x;
                }
                else if (x < mBound.left) {
                    mBound.left = x;
                }
                if (mBound.right === null) {
                    mBound.right = x;
                }
                else if (mBound.right < x) {
                    mBound.right = x;
                }
                if (mBound.bottom === null) {
                    mBound.bottom = y;
                }
                else if (mBound.bottom < y) {
                    mBound.bottom = y;
                }
            }
        }
        const trimHeight = mBound.bottom - mBound.top;
        const trimWidth = mBound.right - mBound.left;
        const trimmed = ctx.getImageData(mBound.left, mBound.top, trimWidth, trimHeight);
        mCanvas.width = trimWidth;
        mCanvas.height = trimHeight;
        ctx.putImageData(trimmed, 0, 0);
        return this;
    }
}
exports.default = ImageControl;
