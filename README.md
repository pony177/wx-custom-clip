# 微信小程序自定义剪裁组件
一个简易的自定义形状剪裁组件。

小蓝阿姨的博客：https://blog.csdn.net/qq_18470967

专业软件开发：https://fuzhu123.taobao.com/

***
## 使用效果

![GIF 2023-2-5 13-26-06](https://sf.gz.cn/static/other/4.gif)

***
## 如何安装
```powershell
# npm方式安装
npm i wx-custom-clip
# yarn方式安装
yarn add wx-custom-clip
```
在根目录中安装完毕依赖后，在微信开发人员工具的菜单栏中点击`工具 - 构建npm`
![sample](https://sf.gz.cn/static/other/7.png)

***
## 使用方法
使用组件很简单，有手就行

```json
"usingComponents": {
  "custom-clip": "/miniprogram_npm/wx-custom-clip/lib/clip-image/index"
}
```

提供了一个简单的例子：https://gitee.com/pony177/wx-custom-clip/tree/master/sample/miniprogram-3

这个例子是一个剪裁例子

![sample](https://sf.gz.cn/static/other/6.png)

直接在例子里头自己学用哈，四个点和线的颜色的改变也写在例子里了，秒懂的那种。

参数不多，传一个src进去，两个绑定方法就搞定了

```html
<clip-image
  wx:if="{{ show }}"
  src="{{ src }}"
  id="clip"
  bind:confirm="onConfirm"
  bind:cancel="onCancel"
 />
```

回调方法`onConfirm`返回值是四个坐标还有图像的url

![image-20230205135242064](https://sf.gz.cn/static/other/5.png)

有了这四个坐标你想继续干啥都可以，这四个坐标分别为`左上，右上，左下，右下`