import ImageControl from './utils/ImageControl'
const TOUCH_LIMIT = 8
const DOT_SIZE = 15
const PADDING_SIZE = {
  top: 30,
  left: 30
}

type ObjectSize = {
  width: number,
  height: number,
  scaleRate: number
}

type DotPointer = {
  name?: string,
  x: number,
  y: number
}

Component({
  properties: {
    src: String,
    style: {
      type: Object,
      value: {
        lineColor: '#FFCA28',
        dotColor: '#FD3960',
        shadowColor: '#FFCA28'
      }
    }
  },

  data: {
    dot: [
      { name: '左上', x: 10, y: 10 }, // 左上
      { name: '右上', x: 100, y: 10 }, // 右上
      { name: '左下', x: 10, y: 100 }, // 左下
      { name: '右下', x: 100, y: 100 }, // 右下
    ] as Array<DotPointer>,
    ctx: {} as WechatMiniprogram.CanvasContext,
    canvas: {} as WechatMiniprogram.Canvas,
    image: {} as any,
    screenSize: {} as ObjectSize,
    imageSize: {} as ObjectSize,
  },

  methods: {
    async onSubmit() {
      const { image, imageSize, dot, screenSize } = this.data
      const mHeightLongRate = image.height / imageSize.height
      const mWidthLongRate = image.width / (imageSize.width - DOT_SIZE)
      const mImagePointer = this.getImagePointer(imageSize, screenSize)
      let mDot = dot.map(item => {
        // 换算大小比例，我需要把图片真正的位置确定出来，然后再进行比例换算
        return {
          x: (item.x - mImagePointer.x) * mWidthLongRate,
          y: (item.y - mImagePointer.y) * mHeightLongRate
        }
      })
      const canvas = wx.createOffscreenCanvas({
        type: '2d',
        width: image.width,
        height: image.height
      }) as any
      const ctx = canvas.getContext('2d')
      ctx.moveTo(mDot[0].x, mDot[0].y)
      ctx.lineTo(mDot[1].x, mDot[1].y)
      ctx.lineTo(mDot[3].x, mDot[3].y)
      ctx.lineTo(mDot[2].x, mDot[2].y)
      ctx.lineTo(mDot[0].x, mDot[0].y)
      ctx.clip()
      ctx.drawImage(image, 0, 0, image.width, image.height)
      const mImageControl = new ImageControl({
        canvas,
        ctx,
        image: ctx.getImageData(0, 0, image.width, image.height)
      })
      const mBase64 = mImageControl.clip().ctx.canvas.toDataURL()
      const fs = wx.getFileSystemManager()
      const mFilePath = `${wx.env.USER_DATA_PATH}/${new Date().getTime()}.jpg`
      fs.writeFileSync(mFilePath, mBase64.slice(22), 'base64')
      this.triggerEvent('confirm', {
        dot: mDot,
        url: mFilePath
      })
    },

    /**
     * 图像圆角算法
     * @param options
     */
    drawRoundRectPath(options: {
      ctx: any,
      width: number,
      height: number,
      radius: number
    }) {
      const { ctx, width, height, radius } = options;
      ctx.beginPath();
      //从右下角顺时针绘制，弧度从0到1/2PI  
      ctx.arc(width - radius, height - radius, radius, 0, Math.PI / 2);
      //矩形下边线  
      ctx.lineTo(radius, height);
      //左下角圆弧，弧度从1/2PI到PI  
      ctx.arc(radius, height - radius, radius, Math.PI / 2, Math.PI);
      //矩形左边线  
      ctx.lineTo(0, radius);
      //左上角圆弧，弧度从PI到3/2PI  
      ctx.arc(radius, radius, radius, Math.PI, Math.PI * 3 / 2);
      //上边线  
      ctx.lineTo(width - radius, 0);
      //右上角圆弧  
      ctx.arc(width - radius, radius, radius, Math.PI * 3 / 2, Math.PI * 2);
      //右边线  
      ctx.lineTo(width, height - radius);
      ctx.clip();
      ctx.closePath();
    },

    onCancel() {
      this.triggerEvent('cancel')
    },

    drawSpace() {
      const { image, imageSize, canvas, ctx, style, screenSize } = this.data
      const mDot = this.data.dot
      canvas.height = screenSize.height
      canvas.width = screenSize.width
      const mImagePointer = this.getImagePointer(imageSize, screenSize)
      ctx.drawImage(
        image,
        mImagePointer.x + DOT_SIZE / 2,
        mImagePointer.y + DOT_SIZE / 2,
        imageSize.width - DOT_SIZE,
        imageSize.height
      )
      // 画四条线
      ctx.moveTo(mDot[0].x + TOUCH_LIMIT, mDot[0].y + TOUCH_LIMIT)
      ctx.lineTo(mDot[1].x + TOUCH_LIMIT, mDot[1].y + TOUCH_LIMIT)
      ctx.lineTo(mDot[3].x + TOUCH_LIMIT, mDot[3].y + TOUCH_LIMIT)
      ctx.lineTo(mDot[2].x + TOUCH_LIMIT, mDot[2].y + TOUCH_LIMIT)
      ctx.lineTo(mDot[0].x + TOUCH_LIMIT, mDot[0].y + TOUCH_LIMIT)
      ctx.lineWidth = 2
      ctx.strokeStyle = style.lineColor || '#FFCA28'
      ctx.stroke()
      // 画四个点
      ctx.fillStyle = style.dotColor || '#FD3960'
      ctx.shadowOffsetX = 2
      ctx.shadowOffsetY = 2
      ctx.shadowBlur = 8
      ctx.shadowColor = style.shadowColor || '#FFCA28'
      ctx.globalAlpha = 0.8
      ctx.fillRect(mDot[0].x, mDot[0].y, DOT_SIZE, DOT_SIZE)
      ctx.fillRect(mDot[1].x, mDot[1].y, DOT_SIZE, DOT_SIZE)
      ctx.fillRect(mDot[2].x, mDot[2].y, DOT_SIZE, DOT_SIZE)
      ctx.fillRect(mDot[3].x, mDot[3].y, DOT_SIZE, DOT_SIZE)
    },

    /**
     * 将坐标转换成点的信息
     * @param x 
     * @param y 
     */
    touchPointToDotIndex(x: number, y: number) {
      const mTouchX = x
      const mTouchY = y
      // 先处理一下点的范围,因为是有TOUCH_LIMIT个像素单位的偏移
      const mDot = this.data.dot.map(dot => ({ name: dot.name, x: dot.x + TOUCH_LIMIT, y: dot.y + TOUCH_LIMIT }))
      // 判断一下点击区域是否在范围内
      for (const i in mDot) {
        // 判断点击位置的算法
        if (Math.abs(mTouchX - mDot[i].x) <= 30 && Math.abs(mTouchY - mDot[i].y) <= 30) {
          return ~~i
        }
      }
      return null
    },

    hitBorder(x: number, y: number) {
      const { screenSize } = this.data
      let mX = x, mY = y
      if (x <= 0) {
        mX = 0
      } else if (x >= screenSize.width - DOT_SIZE) {
        mX = screenSize.width - DOT_SIZE
      }
      if (y <= 0) {
        mY = 0
      } else if (y >= screenSize.height - DOT_SIZE) {
        mY = screenSize.height - DOT_SIZE
      }
      return { x: mX, y: mY }
    },

    onCanvasTouchMove(event: WechatMiniprogram.TouchCanvas) {
      if (event.touches && event.touches.length > 0) {
        const [{ x: mTouchX, y: mTouchY }] = event.touches
        const mPointIndex = this.touchPointToDotIndex(mTouchX, mTouchY)
        if (mPointIndex !== null) {
          const mTouchDot = this.data.dot[mPointIndex]
          if (mTouchDot) {
            this.data.dot[mPointIndex] = {
              ...mTouchDot,
              ...this.hitBorder(mTouchX, mTouchY)
            }
            this.drawSpace()
          }
        }
      }
    },

    getImagePointer(imageSize: ObjectSize, screenSize: ObjectSize) {
      return {
        x: screenSize.width / 2 - imageSize.width / 2,
        y: screenSize.height / 2 - imageSize.height / 2
      }
    }
  },

  lifetimes: {
    attached() {
      const mQuery = this.createSelectorQuery()
      const mDom = mQuery.select('#canvas')
      mDom.node().exec(async (res) => {
        if (res && res[0]) {
          const [{ node: canvas }]: [{ node: WechatMiniprogram.Canvas }] = res
          const ctx = canvas.getContext('2d') as WechatMiniprogram.CanvasContext
          this.data.ctx = ctx
          this.data.canvas = canvas
          // 初始化画布
          const mImage = canvas.createImage()
          mImage.src = this.data.src
          mImage.onload = () => {
            const mSystemInfo = wx.getSystemInfoSync()
            // 获取图片高宽比
            const mImageSize: ObjectSize = {
              width: mImage.width,
              height: mImage.height,
              scaleRate: mImage.height / mImage.width
            }
            // 获取屏幕高宽比
            const mScreenSize: ObjectSize = {
              width: mSystemInfo.windowWidth,
              height: mSystemInfo.windowHeight - 100,
              scaleRate: (mSystemInfo.windowHeight - 100) / (mSystemInfo.windowWidth)
            }

            if (mImageSize.scaleRate < mScreenSize.scaleRate) {
              // 宽度溢出，需要先把
              mImageSize.width = mScreenSize.width - PADDING_SIZE.left
              mImageSize.height = mImageSize.width * mImageSize.scaleRate

              // 如果最后高度还是塞满了，没办法 只能给他缩放一下
              if (mImageSize.height > mScreenSize.height) {
                mImageSize.height = mScreenSize.height - PADDING_SIZE.top
              }
            } else {
              // 高度溢出
              mImageSize.height = mScreenSize.height - PADDING_SIZE.top
              mImageSize.width = mImageSize.height * mImageSize.scaleRate
              // 如果最后宽度还是塞满了，没办法 只能给他缩放一下
              if (mImageSize.width > mScreenSize.width) {
                mImageSize.width = mScreenSize.width - PADDING_SIZE.left
              }
            }
            const mImagePointer = this.getImagePointer(mImageSize, mScreenSize)
            this.setData({
              imageSize: mImageSize,
              screenSize: mScreenSize,
              dot: [
                { name: '左上', x: mImagePointer.x, y: mImagePointer.y }, // 左上
                { name: '右上', x: mImageSize.width, y: mImagePointer.y }, // 右上
                { name: '左下', x: mImagePointer.x, y: mImagePointer.y + mImageSize.height }, // 左下
                { name: '右下', x: mImageSize.width, y: mImagePointer.y + mImageSize.height }, // 右下
              ],
              image: mImage
            }, () => this.drawSpace())
          }
        }
      })
    }
  }
})
