var t, e = require("../../@babel/runtime/helpers/typeof");

t = function() {
    function t(t) {
        return "function" == typeof t;
    }
    var r = Array.isArray ? Array.isArray : function(t) {
        return "[object Array]" === Object.prototype.toString.call(t);
    }, n = 0, o = void 0, i = void 0, s = function(t, e) {
        d[n] = t, d[n + 1] = e, 2 === (n += 2) && (i ? i(v) : b());
    }, u = "undefined" != typeof window ? window : void 0, c = u || {}, a = c.MutationObserver || c.WebKitMutationObserver, f = "undefined" == typeof self && "undefined" != typeof process && "[object process]" === {}.toString.call(process), l = "undefined" != typeof Uint8ClampedArray && "undefined" != typeof importScripts && "undefined" != typeof MessageChannel;
    function h() {
        var t = setTimeout;
        return function() {
            return t(v, 1);
        };
    }
    var d = new Array(1e3);
    function v() {
        for (var t = 0; t < n; t += 2) (0, d[t])(d[t + 1]), d[t] = void 0, d[t + 1] = void 0;
        n = 0;
    }
    var p, _, y, m, b = void 0;
    function w(t, e) {
        var r = arguments, n = this, o = new this.constructor(j);
        void 0 === o[A] && K(o);
        var i, u = n._state;
        return u ? (i = r[u - 1], s(function() {
            return F(u, o, i, n._result);
        })) : O(n, o, t, e), o;
    }
    function g(t) {
        if (t && "object" === e(t) && t.constructor === this) return t;
        var r = new this(j);
        return x(r, t), r;
    }
    f ? b = function() {
        return process.nextTick(v);
    } : a ? (_ = 0, y = new a(v), m = document.createTextNode(""), y.observe(m, {
        characterData: !0
    }), b = function() {
        m.data = _ = ++_ % 2;
    }) : l ? ((p = new MessageChannel()).port1.onmessage = v, b = function() {
        return p.port2.postMessage(0);
    }) : b = void 0 === u && "function" == typeof require ? function() {
        try {
            var t = require("vertx");
            return void 0 !== (o = t.runOnLoop || t.runOnContext) ? function() {
                o(v);
            } : h();
        } catch (t) {
            return h();
        }
    }() : h();
    var A = Math.random().toString(36).substring(16);
    function j() {}
    var S = new Y();
    function E(t) {
        try {
            return t.then;
        } catch (t) {
            return S.error = t, S;
        }
    }
    function T(e, r, n) {
        r.constructor === e.constructor && n === w && r.constructor.resolve === g ? function(t, e) {
            1 === e._state ? P(t, e._result) : 2 === e._state ? C(t, e._result) : O(e, void 0, function(e) {
                return x(t, e);
            }, function(e) {
                return C(t, e);
            });
        }(e, r) : n === S ? C(e, S.error) : void 0 === n ? P(e, r) : t(n) ? function(t, e, r) {
            s(function(t) {
                var n = !1, o = function(t, e, r, n) {
                    try {
                        t.call(e, r, n);
                    } catch (t) {
                        return t;
                    }
                }(r, e, function(r) {
                    n || (n = !0, e !== r ? x(t, r) : P(t, r));
                }, function(e) {
                    n || (n = !0, C(t, e));
                }, t._label);
                !n && o && (n = !0, C(t, o));
            }, t);
        }(e, r, n) : P(e, r);
    }
    function x(t, r) {
        var n;
        t === r ? C(t, new TypeError("You cannot resolve a promise with itself")) : "function" == typeof (n = r) || "object" === e(n) && null !== n ? T(t, r, E(r)) : P(t, r);
    }
    function M(t) {
        t._onerror && t._onerror(t._result), q(t);
    }
    function P(t, e) {
        void 0 === t._state && (t._result = e, t._state = 1, 0 !== t._subscribers.length && s(q, t));
    }
    function C(t, e) {
        void 0 === t._state && (t._state = 2, t._result = e, s(M, t));
    }
    function O(t, e, r, n) {
        var o = t._subscribers, i = o.length;
        t._onerror = null, o[i] = e, o[i + 1] = r, o[i + 2] = n, 0 === i && t._state && s(q, t);
    }
    function q(t) {
        var e = t._subscribers, r = t._state;
        if (0 !== e.length) {
            for (var n = void 0, o = void 0, i = t._result, s = 0; s < e.length; s += 3) n = e[s], 
            o = e[s + r], n ? F(r, n, o, i) : o(i);
            t._subscribers.length = 0;
        }
    }
    function Y() {
        this.error = null;
    }
    var k = new Y();
    function F(e, r, n, o) {
        var i = t(n), s = void 0, u = void 0, c = void 0, a = void 0;
        if (i) {
            if ((s = function(t, e) {
                try {
                    return t(e);
                } catch (t) {
                    return k.error = t, k;
                }
            }(n, o)) === k ? (a = !0, u = s.error, s = null) : c = !0, r === s) return void C(r, new TypeError("A promises callback cannot return that same promise."));
        } else s = o, c = !0;
        void 0 !== r._state || (i && c ? x(r, s) : a ? C(r, u) : 1 === e ? P(r, s) : 2 === e && C(r, s));
    }
    var D = 0;
    function K(t) {
        t[A] = D++, t._state = void 0, t._result = void 0, t._subscribers = [];
    }
    function L(t, e) {
        this._instanceConstructor = t, this.promise = new t(j), this.promise[A] || K(this.promise), 
        r(e) ? (this._input = e, this.length = e.length, this._remaining = e.length, this._result = new Array(this.length), 
        0 === this.length ? P(this.promise, this._result) : (this.length = this.length || 0, 
        this._enumerate(), 0 === this._remaining && P(this.promise, this._result))) : C(this.promise, new Error("Array Methods must be provided an Array"));
    }
    function N(t) {
        this[A] = D++, this._result = this._state = void 0, this._subscribers = [], j !== t && ("function" != typeof t && function() {
            throw new TypeError("You must pass a resolver function as the first argument to the promise constructor");
        }(), this instanceof N ? function(t, e) {
            try {
                e(function(e) {
                    x(t, e);
                }, function(e) {
                    C(t, e);
                });
            } catch (e) {
                C(t, e);
            }
        }(this, t) : function() {
            throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
        }());
    }
    return L.prototype._enumerate = function() {
        for (var t = this.length, e = this._input, r = 0; void 0 === this._state && r < t; r++) this._eachEntry(e[r], r);
    }, L.prototype._eachEntry = function(t, e) {
        var r = this._instanceConstructor, n = r.resolve;
        if (n === g) {
            var o = E(t);
            if (o === w && void 0 !== t._state) this._settledAt(t._state, e, t._result); else if ("function" != typeof o) this._remaining--, 
            this._result[e] = t; else if (r === N) {
                var i = new r(j);
                T(i, t, o), this._willSettleAt(i, e);
            } else this._willSettleAt(new r(function(e) {
                return e(t);
            }), e);
        } else this._willSettleAt(n(t), e);
    }, L.prototype._settledAt = function(t, e, r) {
        var n = this.promise;
        void 0 === n._state && (this._remaining--, 2 === t ? C(n, r) : this._result[e] = r), 
        0 === this._remaining && P(n, this._result);
    }, L.prototype._willSettleAt = function(t, e) {
        var r = this;
        O(t, void 0, function(t) {
            return r._settledAt(1, e, t);
        }, function(t) {
            return r._settledAt(2, e, t);
        });
    }, N.all = function(t) {
        return new L(this, t).promise;
    }, N.race = function(t) {
        var e = this;
        return r(t) ? new e(function(r, n) {
            for (var o = t.length, i = 0; i < o; i++) e.resolve(t[i]).then(r, n);
        }) : new e(function(t, e) {
            return e(new TypeError("You must pass an array to race."));
        });
    }, N.resolve = g, N.reject = function(t) {
        var e = new this(j);
        return C(e, t), e;
    }, N._setScheduler = function(t) {
        i = t;
    }, N._setAsap = function(t) {
        s = t;
    }, N._asap = s, N.prototype = {
        constructor: N,
        then: w,
        catch: function(t) {
            return this.then(null, t);
        }
    }, N.polyfill = function() {
        var t = void 0;
        if ("undefined" != typeof global) t = global; else if ("undefined" != typeof self) t = self; else try {
            t = Function("return this")();
        } catch (t) {
            throw new Error("polyfill failed because global object is unavailable in this environment");
        }
        var e = t.Promise;
        if (e) {
            var r = null;
            try {
                r = Object.prototype.toString.call(e.resolve());
            } catch (t) {}
            if ("[object Promise]" === r && !e.cast) return;
        }
        t.Promise = N;
    }, N.Promise = N, N;
}, "object" === ("undefined" == typeof exports ? "undefined" : e(exports)) && "undefined" != typeof module ? module.exports = t() : "function" == typeof define && define.amd ? define(t) : (void 0).ES6Promise = t();