var t = Array.prototype.slice, n = require("../es6-promise/es6-promise").Promise;

function r(r) {
    var o = this, u = t.call(arguments, 1);
    return new n(function(t, n) {
        if ("function" == typeof r && (r = r.apply(o, u)), !r || "function" != typeof r.next) return t(r);
        function c(t) {
            var e;
            try {
                e = r.next(t);
            } catch (t) {
                return n(t);
            }
            l(e);
        }
        function a(t) {
            var e;
            try {
                e = r.throw(t);
            } catch (t) {
                return n(t);
            }
            l(e);
        }
        function l(n) {
            if (n.done) return t(n.value);
            var r = e.call(o, n.value);
            return r && i(r) ? r.then(c, a) : a(new TypeError('You may only yield a function, promise, generator, array, or object, but the following object was passed: "' + String(n.value) + '"'));
        }
        c();
    });
}

function e(t) {
    return t ? i(t) ? t : function(t) {
        var n = t.constructor;
        return !!n && ("GeneratorFunction" === n.name || "GeneratorFunction" === n.displayName || a(n.prototype));
    }(t) || a(t) ? r.call(this, t) : "function" == typeof t ? o.call(this, t) : Array.isArray(t) ? u.call(this, t) : Object == t.constructor ? c.call(this, t) : t : t;
}

function o(r) {
    var e = this;
    return new n(function(n, o) {
        r.call(e, function(r, e) {
            if (r) return o(r);
            arguments.length > 2 && (e = t.call(arguments, 1)), n(e);
        });
    });
}

function u(t) {
    return n.all(t.map(e, this));
}

function c(t) {
    for (var r = new t.constructor(), o = Object.keys(t), u = [], c = 0; c < o.length; c++) {
        var a = o[c], l = e.call(this, t[a]);
        l && i(l) ? f(l, a) : r[a] = t[a];
    }
    return n.all(u).then(function() {
        return r;
    });
    function f(t, n) {
        r[n] = void 0, u.push(t.then(function(t) {
            r[n] = t;
        }));
    }
}

function i(t) {
    return "function" == typeof t.then;
}

function a(t) {
    return "function" == typeof t.next && "function" == typeof t.throw;
}

module.exports = r.default = r.co = r, r.wrap = function(t) {
    return n.__generatorFunction__ = t, n;
    function n() {
        return r.call(this, t.apply(this, arguments));
    }
};