var t = require("../../@babel/runtime/helpers/typeof");

!function(e) {
    var r = Object.prototype, n = r.hasOwnProperty, o = "function" == typeof Symbol ? Symbol : {}, i = o.iterator || "@@iterator", a = o.toStringTag || "@@toStringTag", c = "object" === ("undefined" == typeof module ? "undefined" : t(module)), u = e.regeneratorRuntime;
    if (u) c && (module.exports = u); else {
        (u = e.regeneratorRuntime = c ? module.exports : {}).wrap = d;
        var f = {}, l = {};
        l[i] = function() {
            return this;
        };
        var s = Object.getPrototypeOf, h = s && s(s(_([])));
        h && h !== r && n.call(h, i) && (l = h);
        var p = w.prototype = v.prototype = Object.create(l);
        g.prototype = p.constructor = w, w.constructor = g, w[a] = g.displayName = "GeneratorFunction", 
        u.isGeneratorFunction = function(t) {
            var e = "function" == typeof t && t.constructor;
            return !!e && (e === g || "GeneratorFunction" === (e.displayName || e.name));
        }, u.mark = function(t) {
            return Object.setPrototypeOf ? Object.setPrototypeOf(t, w) : (t.__proto__ = w, a in t || (t[a] = "GeneratorFunction")), 
            t.prototype = Object.create(p), t;
        }, u.awrap = function(t) {
            return {
                __await: t
            };
        }, m(L.prototype), u.AsyncIterator = L, u.async = function(t, e, r, n) {
            var o = new L(d(t, e, r, n));
            return u.isGeneratorFunction(e) ? o : o.next().then(function(t) {
                return t.done ? t.value : o.next();
            });
        }, m(p), p[a] = "Generator", p.toString = function() {
            return "[object Generator]";
        }, u.keys = function(t) {
            var e = [];
            for (var r in t) e.push(r);
            return e.reverse(), function r() {
                for (;e.length; ) {
                    var n = e.pop();
                    if (n in t) return r.value = n, r.done = !1, r;
                }
                return r.done = !0, r;
            };
        }, u.values = _, E.prototype = {
            constructor: E,
            reset: function(t) {
                if (this.prev = 0, this.next = 0, this.sent = this._sent = void 0, this.done = !1, 
                this.delegate = null, this.tryEntries.forEach(x), !t) for (var e in this) "t" === e.charAt(0) && n.call(this, e) && !isNaN(+e.slice(1)) && (this[e] = void 0);
            },
            stop: function() {
                this.done = !0;
                var t = this.tryEntries[0].completion;
                if ("throw" === t.type) throw t.arg;
                return this.rval;
            },
            dispatchException: function(t) {
                if (this.done) throw t;
                var e = this;
                function r(r, n) {
                    return a.type = "throw", a.arg = t, e.next = r, !!n;
                }
                for (var o = this.tryEntries.length - 1; o >= 0; --o) {
                    var i = this.tryEntries[o], a = i.completion;
                    if ("root" === i.tryLoc) return r("end");
                    if (i.tryLoc <= this.prev) {
                        var c = n.call(i, "catchLoc"), u = n.call(i, "finallyLoc");
                        if (c && u) {
                            if (this.prev < i.catchLoc) return r(i.catchLoc, !0);
                            if (this.prev < i.finallyLoc) return r(i.finallyLoc);
                        } else if (c) {
                            if (this.prev < i.catchLoc) return r(i.catchLoc, !0);
                        } else {
                            if (!u) throw new Error("try statement without catch or finally");
                            if (this.prev < i.finallyLoc) return r(i.finallyLoc);
                        }
                    }
                }
            },
            abrupt: function(t, e) {
                for (var r = this.tryEntries.length - 1; r >= 0; --r) {
                    var o = this.tryEntries[r];
                    if (o.tryLoc <= this.prev && n.call(o, "finallyLoc") && this.prev < o.finallyLoc) {
                        var i = o;
                        break;
                    }
                }
                i && ("break" === t || "continue" === t) && i.tryLoc <= e && e <= i.finallyLoc && (i = null);
                var a = i ? i.completion : {};
                return a.type = t, a.arg = e, i ? this.next = i.finallyLoc : this.complete(a), f;
            },
            complete: function(t, e) {
                if ("throw" === t.type) throw t.arg;
                "break" === t.type || "continue" === t.type ? this.next = t.arg : "return" === t.type ? (this.rval = t.arg, 
                this.next = "end") : "normal" === t.type && e && (this.next = e);
            },
            finish: function(t) {
                for (var e = this.tryEntries.length - 1; e >= 0; --e) {
                    var r = this.tryEntries[e];
                    if (r.finallyLoc === t) return this.complete(r.completion, r.afterLoc), x(r), f;
                }
            },
            catch: function(t) {
                for (var e = this.tryEntries.length - 1; e >= 0; --e) {
                    var r = this.tryEntries[e];
                    if (r.tryLoc === t) {
                        var n = r.completion;
                        if ("throw" === n.type) {
                            var o = n.arg;
                            x(r);
                        }
                        return o;
                    }
                }
                throw new Error("illegal catch attempt");
            },
            delegateYield: function(t, e, r) {
                return this.delegate = {
                    iterator: _(t),
                    resultName: e,
                    nextLoc: r
                }, f;
            }
        };
    }
    function d(t, e, r, n) {
        var o = e && e.prototype instanceof v ? e : v, i = Object.create(o.prototype), a = new E(n || []);
        return i._invoke = function(t, e, r) {
            var n = "suspendedStart";
            return function(o, i) {
                if ("executing" === n) throw new Error("Generator is already running");
                if ("completed" === n) {
                    if ("throw" === o) throw i;
                    return j();
                }
                for (;;) {
                    var a = r.delegate;
                    if (a) {
                        if ("return" === o || "throw" === o && void 0 === a.iterator[o]) {
                            r.delegate = null;
                            var c = a.iterator.return;
                            if (c) if ("throw" === (u = y(c, a.iterator, i)).type) {
                                o = "throw", i = u.arg;
                                continue;
                            }
                            if ("return" === o) continue;
                        }
                        var u;
                        if ("throw" === (u = y(a.iterator[o], a.iterator, i)).type) {
                            r.delegate = null, o = "throw", i = u.arg;
                            continue;
                        }
                        if (o = "next", i = void 0, !(l = u.arg).done) return n = "suspendedYield", l;
                        r[a.resultName] = l.value, r.next = a.nextLoc, r.delegate = null;
                    }
                    if ("next" === o) r.sent = r._sent = i; else if ("throw" === o) {
                        if ("suspendedStart" === n) throw n = "completed", i;
                        r.dispatchException(i) && (o = "next", i = void 0);
                    } else "return" === o && r.abrupt("return", i);
                    if (n = "executing", "normal" === (u = y(t, e, r)).type) {
                        n = r.done ? "completed" : "suspendedYield";
                        var l = {
                            value: u.arg,
                            done: r.done
                        };
                        if (u.arg !== f) return l;
                        r.delegate && "next" === o && (i = void 0);
                    } else "throw" === u.type && (n = "completed", o = "throw", i = u.arg);
                }
            };
        }(t, r, a), i;
    }
    function y(t, e, r) {
        try {
            return {
                type: "normal",
                arg: t.call(e, r)
            };
        } catch (t) {
            return {
                type: "throw",
                arg: t
            };
        }
    }
    function v() {}
    function g() {}
    function w() {}
    function m(t) {
        [ "next", "throw", "return" ].forEach(function(e) {
            t[e] = function(t) {
                return this._invoke(e, t);
            };
        });
    }
    function L(e) {
        function r(o, i, a, c) {
            var u = y(e[o], e, i);
            if ("throw" !== u.type) {
                var f = u.arg, l = f.value;
                return l && "object" === t(l) && n.call(l, "__await") ? Promise.resolve(l.__await).then(function(t) {
                    r("next", t, a, c);
                }, function(t) {
                    r("throw", t, a, c);
                }) : Promise.resolve(l).then(function(t) {
                    f.value = t, a(f);
                }, c);
            }
            c(u.arg);
        }
        var o;
        "object" === ("undefined" == typeof process ? "undefined" : t(process)) && process.domain && (r = process.domain.bind(r)), 
        this._invoke = function(t, e) {
            function n() {
                return new Promise(function(n, o) {
                    r(t, e, n, o);
                });
            }
            return o = o ? o.then(n, n) : n();
        };
    }
    function b(t) {
        var e = {
            tryLoc: t[0]
        };
        1 in t && (e.catchLoc = t[1]), 2 in t && (e.finallyLoc = t[2], e.afterLoc = t[3]), 
        this.tryEntries.push(e);
    }
    function x(t) {
        var e = t.completion || {};
        e.type = "normal", delete e.arg, t.completion = e;
    }
    function E(t) {
        this.tryEntries = [ {
            tryLoc: "root"
        } ], t.forEach(b, this), this.reset(!0);
    }
    function _(t) {
        if (t) {
            var e = t[i];
            if (e) return e.call(t);
            if ("function" == typeof t.next) return t;
            if (!isNaN(t.length)) {
                var r = -1, o = function e() {
                    for (;++r < t.length; ) if (n.call(t, r)) return e.value = t[r], e.done = !1, e;
                    return e.value = void 0, e.done = !0, e;
                };
                return o.next = o;
            }
        }
        return {
            next: j
        };
    }
    function j() {
        return {
            value: void 0,
            done: !0
        };
    }
}("object" === ("undefined" == typeof global ? "undefined" : t(global)) ? global : "object" === ("undefined" == typeof window ? "undefined" : t(window)) ? window : "object" === ("undefined" == typeof self ? "undefined" : t(self)) ? self : void 0);