var n = require("../../@babel/runtime/helpers/typeof");

require("../../@babel/runtime/helpers/Arrayincludes"), function() {
    var r = Array.prototype, t = Object.prototype, e = Function.prototype, u = r.push, i = r.slice, o = t.toString, a = t.hasOwnProperty, c = Array.isArray, f = Object.keys, l = e.bind, s = Object.create, p = function() {}, h = function n(r) {
        return r instanceof n ? r : this instanceof n ? void (this._wrapped = r) : new n(r);
    };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = h), 
    exports._ = h) : root._ = h, h.VERSION = "1.8.3";
    var v = function(n, r, t) {
        if (void 0 === r) return n;
        switch (null == t ? 3 : t) {
          case 1:
            return function(t) {
                return n.call(r, t);
            };

          case 2:
            return function(t, e) {
                return n.call(r, t, e);
            };

          case 3:
            return function(t, e, u) {
                return n.call(r, t, e, u);
            };

          case 4:
            return function(t, e, u, i) {
                return n.call(r, t, e, u, i);
            };
        }
        return function() {
            return n.apply(r, arguments);
        };
    }, y = function(n, r, t) {
        return null == n ? h.identity : h.isFunction(n) ? v(n, r, t) : h.isObject(n) ? h.matcher(n) : h.property(n);
    };
    h.iteratee = function(n, r) {
        return y(n, r, 1 / 0);
    };
    var d = function(n, r) {
        return function(t) {
            var e = arguments.length;
            if (e < 2 || null == t) return t;
            for (var u = 1; u < e; u++) for (var i = arguments[u], o = n(i), a = o.length, c = 0; c < a; c++) {
                var f = o[c];
                r && void 0 !== t[f] || (t[f] = i[f]);
            }
            return t;
        };
    }, g = function(n) {
        if (!h.isObject(n)) return {};
        if (s) return s(n);
        p.prototype = n;
        var r = new p();
        return p.prototype = null, r;
    }, m = function(n) {
        return function(r) {
            return null == r ? void 0 : r[n];
        };
    }, b = Math.pow(2, 53) - 1, x = m("length"), j = function(n) {
        var r = x(n);
        return "number" == typeof r && r >= 0 && r <= b;
    };
    function _(n) {
        function r(r, t, e, u, i, o) {
            for (;i >= 0 && i < o; i += n) {
                var a = u ? u[i] : i;
                e = t(e, r[a], a, r);
            }
            return e;
        }
        return function(t, e, u, i) {
            e = v(e, i, 4);
            var o = !j(t) && h.keys(t), a = (o || t).length, c = n > 0 ? 0 : a - 1;
            return arguments.length < 3 && (u = t[o ? o[c] : c], c += n), r(t, e, u, o, c, a);
        };
    }
    h.each = h.forEach = function(n, r, t) {
        var e, u;
        if (r = v(r, t), j(n)) for (e = 0, u = n.length; e < u; e++) r(n[e], e, n); else {
            var i = h.keys(n);
            for (e = 0, u = i.length; e < u; e++) r(n[i[e]], i[e], n);
        }
        return n;
    }, h.map = h.collect = function(n, r, t) {
        r = y(r, t);
        for (var e = !j(n) && h.keys(n), u = (e || n).length, i = Array(u), o = 0; o < u; o++) {
            var a = e ? e[o] : o;
            i[o] = r(n[a], a, n);
        }
        return i;
    }, h.reduce = h.foldl = h.inject = _(1), h.reduceRight = h.foldr = _(-1), h.find = h.detect = function(n, r, t) {
        var e;
        if (void 0 !== (e = j(n) ? h.findIndex(n, r, t) : h.findKey(n, r, t)) && -1 !== e) return n[e];
    }, h.filter = h.select = function(n, r, t) {
        var e = [];
        return r = y(r, t), h.each(n, function(n, t, u) {
            r(n, t, u) && e.push(n);
        }), e;
    }, h.reject = function(n, r, t) {
        return h.filter(n, h.negate(y(r)), t);
    }, h.every = h.all = function(n, r, t) {
        r = y(r, t);
        for (var e = !j(n) && h.keys(n), u = (e || n).length, i = 0; i < u; i++) {
            var o = e ? e[i] : i;
            if (!r(n[o], o, n)) return !1;
        }
        return !0;
    }, h.some = h.any = function(n, r, t) {
        r = y(r, t);
        for (var e = !j(n) && h.keys(n), u = (e || n).length, i = 0; i < u; i++) {
            var o = e ? e[i] : i;
            if (r(n[o], o, n)) return !0;
        }
        return !1;
    }, h.contains = h.includes = h.include = function(n, r, t, e) {
        return j(n) || (n = h.values(n)), ("number" != typeof t || e) && (t = 0), h.indexOf(n, r, t) >= 0;
    }, h.invoke = function(n, r) {
        var t = i.call(arguments, 2), e = h.isFunction(r);
        return h.map(n, function(n) {
            var u = e ? r : n[r];
            return null == u ? u : u.apply(n, t);
        });
    }, h.pluck = function(n, r) {
        return h.map(n, h.property(r));
    }, h.where = function(n, r) {
        return h.filter(n, h.matcher(r));
    }, h.findWhere = function(n, r) {
        return h.find(n, h.matcher(r));
    }, h.max = function(n, r, t) {
        var e, u, i = -1 / 0, o = -1 / 0;
        if (null == r && null != n) for (var a = 0, c = (n = j(n) ? n : h.values(n)).length; a < c; a++) (e = n[a]) > i && (i = e); else r = y(r, t), 
        h.each(n, function(n, t, e) {
            ((u = r(n, t, e)) > o || u === -1 / 0 && i === -1 / 0) && (i = n, o = u);
        });
        return i;
    }, h.min = function(n, r, t) {
        var e, u, i = 1 / 0, o = 1 / 0;
        if (null == r && null != n) for (var a = 0, c = (n = j(n) ? n : h.values(n)).length; a < c; a++) (e = n[a]) < i && (i = e); else r = y(r, t), 
        h.each(n, function(n, t, e) {
            ((u = r(n, t, e)) < o || u === 1 / 0 && i === 1 / 0) && (i = n, o = u);
        });
        return i;
    }, h.shuffle = function(n) {
        for (var r, t = j(n) ? n : h.values(n), e = t.length, u = Array(e), i = 0; i < e; i++) (r = h.random(0, i)) !== i && (u[i] = u[r]), 
        u[r] = t[i];
        return u;
    }, h.sample = function(n, r, t) {
        return null == r || t ? (j(n) || (n = h.values(n)), n[h.random(n.length - 1)]) : h.shuffle(n).slice(0, Math.max(0, r));
    }, h.sortBy = function(n, r, t) {
        return r = y(r, t), h.pluck(h.map(n, function(n, t, e) {
            return {
                value: n,
                index: t,
                criteria: r(n, t, e)
            };
        }).sort(function(n, r) {
            var t = n.criteria, e = r.criteria;
            if (t !== e) {
                if (t > e || void 0 === t) return 1;
                if (t < e || void 0 === e) return -1;
            }
            return n.index - r.index;
        }), "value");
    };
    var w = function(n) {
        return function(r, t, e) {
            var u = {};
            return t = y(t, e), h.each(r, function(e, i) {
                var o = t(e, i, r);
                n(u, e, o);
            }), u;
        };
    };
    h.groupBy = w(function(n, r, t) {
        h.has(n, t) ? n[t].push(r) : n[t] = [ r ];
    }), h.indexBy = w(function(n, r, t) {
        n[t] = r;
    }), h.countBy = w(function(n, r, t) {
        h.has(n, t) ? n[t]++ : n[t] = 1;
    }), h.toArray = function(n) {
        return n ? h.isArray(n) ? i.call(n) : j(n) ? h.map(n, h.identity) : h.values(n) : [];
    }, h.size = function(n) {
        return null == n ? 0 : j(n) ? n.length : h.keys(n).length;
    }, h.partition = function(n, r, t) {
        r = y(r, t);
        var e = [], u = [];
        return h.each(n, function(n, t, i) {
            (r(n, t, i) ? e : u).push(n);
        }), [ e, u ];
    }, h.first = h.head = h.take = function(n, r, t) {
        if (null != n) return null == r || t ? n[0] : h.initial(n, n.length - r);
    }, h.initial = function(n, r, t) {
        return i.call(n, 0, Math.max(0, n.length - (null == r || t ? 1 : r)));
    }, h.last = function(n, r, t) {
        if (null != n) return null == r || t ? n[n.length - 1] : h.rest(n, Math.max(0, n.length - r));
    }, h.rest = h.tail = h.drop = function(n, r, t) {
        return i.call(n, null == r || t ? 1 : r);
    }, h.compact = function(n) {
        return h.filter(n, h.identity);
    };
    var A = function n(r, t, e, u) {
        for (var i = [], o = 0, a = u || 0, c = x(r); a < c; a++) {
            var f = r[a];
            if (j(f) && (h.isArray(f) || h.isArguments(f))) {
                t || (f = n(f, t, e));
                var l = 0, s = f.length;
                for (i.length += s; l < s; ) i[o++] = f[l++];
            } else e || (i[o++] = f);
        }
        return i;
    };
    function O(n) {
        return function(r, t, e) {
            t = y(t, e);
            for (var u = x(r), i = n > 0 ? 0 : u - 1; i >= 0 && i < u; i += n) if (t(r[i], i, r)) return i;
            return -1;
        };
    }
    function k(n, r, t) {
        return function(e, u, o) {
            var a = 0, c = x(e);
            if ("number" == typeof o) n > 0 ? a = o >= 0 ? o : Math.max(o + c, a) : c = o >= 0 ? Math.min(o + 1, c) : o + c + 1; else if (t && o && c) return e[o = t(e, u)] === u ? o : -1;
            if (u != u) return (o = r(i.call(e, a, c), h.isNaN)) >= 0 ? o + a : -1;
            for (o = n > 0 ? a : c - 1; o >= 0 && o < c; o += n) if (e[o] === u) return o;
            return -1;
        };
    }
    h.flatten = function(n, r) {
        return A(n, r, !1);
    }, h.without = function(n) {
        return h.difference(n, i.call(arguments, 1));
    }, h.uniq = h.unique = function(n, r, t, e) {
        h.isBoolean(r) || (e = t, t = r, r = !1), null != t && (t = y(t, e));
        for (var u = [], i = [], o = 0, a = x(n); o < a; o++) {
            var c = n[o], f = t ? t(c, o, n) : c;
            r ? (o && i === f || u.push(c), i = f) : t ? h.contains(i, f) || (i.push(f), u.push(c)) : h.contains(u, c) || u.push(c);
        }
        return u;
    }, h.union = function() {
        return h.uniq(A(arguments, !0, !0));
    }, h.intersection = function(n) {
        for (var r = [], t = arguments.length, e = 0, u = x(n); e < u; e++) {
            var i = n[e];
            if (!h.contains(r, i)) {
                for (var o = 1; o < t && h.contains(arguments[o], i); o++) ;
                o === t && r.push(i);
            }
        }
        return r;
    }, h.difference = function(n) {
        var r = A(arguments, !0, !0, 1);
        return h.filter(n, function(n) {
            return !h.contains(r, n);
        });
    }, h.zip = function() {
        return h.unzip(arguments);
    }, h.unzip = function(n) {
        for (var r = n && h.max(n, x).length || 0, t = Array(r), e = 0; e < r; e++) t[e] = h.pluck(n, e);
        return t;
    }, h.object = function(n, r) {
        for (var t = {}, e = 0, u = x(n); e < u; e++) r ? t[n[e]] = r[e] : t[n[e][0]] = n[e][1];
        return t;
    }, h.findIndex = O(1), h.findLastIndex = O(-1), h.sortedIndex = function(n, r, t, e) {
        for (var u = (t = y(t, e, 1))(r), i = 0, o = x(n); i < o; ) {
            var a = Math.floor((i + o) / 2);
            t(n[a]) < u ? i = a + 1 : o = a;
        }
        return i;
    }, h.indexOf = k(1, h.findIndex, h.sortedIndex), h.lastIndexOf = k(-1, h.findLastIndex), 
    h.range = function(n, r, t) {
        null == r && (r = n || 0, n = 0), t = t || 1;
        for (var e = Math.max(Math.ceil((r - n) / t), 0), u = Array(e), i = 0; i < e; i++, 
        n += t) u[i] = n;
        return u;
    };
    var E = function(n, r, t, e, u) {
        if (!(e instanceof r)) return n.apply(t, u);
        var i = g(n.prototype), o = n.apply(i, u);
        return h.isObject(o) ? o : i;
    };
    h.bind = function(n, r) {
        if (l && n.bind === l) return l.apply(n, i.call(arguments, 1));
        if (!h.isFunction(n)) throw new TypeError("Bind must be called on a function");
        var t = i.call(arguments, 2), e = function e() {
            return E(n, e, r, this, t.concat(i.call(arguments)));
        };
        return e;
    }, h.partial = function(n) {
        var r = i.call(arguments, 1), t = function t() {
            for (var e = 0, u = r.length, i = Array(u), o = 0; o < u; o++) i[o] = r[o] === h ? arguments[e++] : r[o];
            for (;e < arguments.length; ) i.push(arguments[e++]);
            return E(n, t, this, this, i);
        };
        return t;
    }, h.bindAll = function(n) {
        var r, t, e = arguments.length;
        if (e <= 1) throw new Error("bindAll must be passed function names");
        for (r = 1; r < e; r++) n[t = arguments[r]] = h.bind(n[t], n);
        return n;
    }, h.memoize = function(n, r) {
        var t = function t(e) {
            var u = t.cache, i = "" + (r ? r.apply(this, arguments) : e);
            return h.has(u, i) || (u[i] = n.apply(this, arguments)), u[i];
        };
        return t.cache = {}, t;
    }, h.delay = function(n, r) {
        var t = i.call(arguments, 2);
        return setTimeout(function() {
            return n.apply(null, t);
        }, r);
    }, h.defer = h.partial(h.delay, h, 1), h.throttle = function(n, r, t) {
        var e, u, i, o = null, a = 0;
        t || (t = {});
        var c = function() {
            a = !1 === t.leading ? 0 : h.now(), o = null, i = n.apply(e, u), o || (e = u = null);
        };
        return function() {
            var f = h.now();
            a || !1 !== t.leading || (a = f);
            var l = r - (f - a);
            return e = this, u = arguments, l <= 0 || l > r ? (o && (clearTimeout(o), o = null), 
            a = f, i = n.apply(e, u), o || (e = u = null)) : o || !1 === t.trailing || (o = setTimeout(c, l)), 
            i;
        };
    }, h.debounce = function(n, r, t) {
        var e, u, i, o, a, c = function c() {
            var f = h.now() - o;
            f < r && f >= 0 ? e = setTimeout(c, r - f) : (e = null, t || (a = n.apply(i, u), 
            e || (i = u = null)));
        };
        return function() {
            i = this, u = arguments, o = h.now();
            var f = t && !e;
            return e || (e = setTimeout(c, r)), f && (a = n.apply(i, u), i = u = null), a;
        };
    }, h.wrap = function(n, r) {
        return h.partial(r, n);
    }, h.negate = function(n) {
        return function() {
            return !n.apply(this, arguments);
        };
    }, h.compose = function() {
        var n = arguments, r = n.length - 1;
        return function() {
            for (var t = r, e = n[r].apply(this, arguments); t--; ) e = n[t].call(this, e);
            return e;
        };
    }, h.after = function(n, r) {
        return function() {
            if (--n < 1) return r.apply(this, arguments);
        };
    }, h.before = function(n, r) {
        var t;
        return function() {
            return --n > 0 && (t = r.apply(this, arguments)), n <= 1 && (r = null), t;
        };
    }, h.once = h.partial(h.before, 2);
    var F = !{
        toString: null
    }.propertyIsEnumerable("toString"), S = [ "valueOf", "isPrototypeOf", "toString", "propertyIsEnumerable", "hasOwnProperty", "toLocaleString" ];
    function I(n, r) {
        var e = S.length, u = n.constructor, i = h.isFunction(u) && u.prototype || t, o = "constructor";
        for (h.has(n, o) && !h.contains(r, o) && r.push(o); e--; ) (o = S[e]) in n && n[o] !== i[o] && !h.contains(r, o) && r.push(o);
    }
    h.keys = function(n) {
        if (!h.isObject(n)) return [];
        if (f) return f(n);
        var r = [];
        for (var t in n) h.has(n, t) && r.push(t);
        return F && I(n, r), r;
    }, h.allKeys = function(n) {
        if (!h.isObject(n)) return [];
        var r = [];
        for (var t in n) r.push(t);
        return F && I(n, r), r;
    }, h.values = function(n) {
        for (var r = h.keys(n), t = r.length, e = Array(t), u = 0; u < t; u++) e[u] = n[r[u]];
        return e;
    }, h.mapObject = function(n, r, t) {
        r = y(r, t);
        for (var e, u = h.keys(n), i = u.length, o = {}, a = 0; a < i; a++) o[e = u[a]] = r(n[e], e, n);
        return o;
    }, h.pairs = function(n) {
        for (var r = h.keys(n), t = r.length, e = Array(t), u = 0; u < t; u++) e[u] = [ r[u], n[r[u]] ];
        return e;
    }, h.invert = function(n) {
        for (var r = {}, t = h.keys(n), e = 0, u = t.length; e < u; e++) r[n[t[e]]] = t[e];
        return r;
    }, h.functions = h.methods = function(n) {
        var r = [];
        for (var t in n) h.isFunction(n[t]) && r.push(t);
        return r.sort();
    }, h.extend = d(h.allKeys), h.extendOwn = h.assign = d(h.keys), h.findKey = function(n, r, t) {
        r = y(r, t);
        for (var e, u = h.keys(n), i = 0, o = u.length; i < o; i++) if (r(n[e = u[i]], e, n)) return e;
    }, h.pick = function(n, r, t) {
        var e, u, i = {}, o = n;
        if (null == o) return i;
        h.isFunction(r) ? (u = h.allKeys(o), e = v(r, t)) : (u = A(arguments, !1, !1, 1), 
        e = function(n, r, t) {
            return r in t;
        }, o = Object(o));
        for (var a = 0, c = u.length; a < c; a++) {
            var f = u[a], l = o[f];
            e(l, f, o) && (i[f] = l);
        }
        return i;
    }, h.omit = function(n, r, t) {
        if (h.isFunction(r)) r = h.negate(r); else {
            var e = h.map(A(arguments, !1, !1, 1), String);
            r = function(n, r) {
                return !h.contains(e, r);
            };
        }
        return h.pick(n, r, t);
    }, h.defaults = d(h.allKeys, !0), h.create = function(n, r) {
        var t = g(n);
        return r && h.extendOwn(t, r), t;
    }, h.clone = function(n) {
        return h.isObject(n) ? h.isArray(n) ? n.slice() : h.extend({}, n) : n;
    }, h.deepClone = function(n) {
        var r;
        if (!h.isObject(n) || "function" == typeof n) return n;
        for (var t in r = h.isArray(n) ? [] : {}, n) n.hasOwnProperty(t) && (h.isObject(n[t]) ? r[t] = h.deepClone(n[t]) : r[t] = n[t]);
        return r;
    }, h.tap = function(n, r) {
        return r(n), n;
    }, h.isMatch = function(n, r) {
        var t = h.keys(r), e = t.length;
        if (null == n) return !e;
        for (var u = Object(n), i = 0; i < e; i++) {
            var o = t[i];
            if (r[o] !== u[o] || !(o in u)) return !1;
        }
        return !0;
    };
    var M = function r(t, e, u, i) {
        if (t === e) return 0 !== t || 1 / t == 1 / e;
        if (null == t || null == e) return t === e;
        t instanceof h && (t = t._wrapped), e instanceof h && (e = e._wrapped);
        var a = o.call(t);
        if (a !== o.call(e)) return !1;
        switch (a) {
          case "[object RegExp]":
          case "[object String]":
            return "" + t == "" + e;

          case "[object Number]":
            return +t != +t ? +e != +e : 0 == +t ? 1 / +t == 1 / e : +t == +e;

          case "[object Date]":
          case "[object Boolean]":
            return +t == +e;
        }
        var c = "[object Array]" === a;
        if (!c) {
            if ("object" != n(t) || "object" != n(e)) return !1;
            var f = t.constructor, l = e.constructor;
            if (f !== l && !(h.isFunction(f) && f instanceof f && h.isFunction(l) && l instanceof l) && "constructor" in t && "constructor" in e) return !1;
        }
        i = i || [];
        for (var s = (u = u || []).length; s--; ) if (u[s] === t) return i[s] === e;
        if (u.push(t), i.push(e), c) {
            if ((s = t.length) !== e.length) return !1;
            for (;s--; ) if (!r(t[s], e[s], u, i)) return !1;
        } else {
            var p, v = h.keys(t);
            if (s = v.length, h.keys(e).length !== s) return !1;
            for (;s--; ) if (p = v[s], !h.has(e, p) || !r(t[p], e[p], u, i)) return !1;
        }
        return u.pop(), i.pop(), !0;
    };
    h.isEqual = function(n, r) {
        return M(n, r);
    }, h.isEmpty = function(n) {
        return null == n || (j(n) && (h.isArray(n) || h.isString(n) || h.isArguments(n)) ? 0 === n.length : 0 === h.keys(n).length);
    }, h.isNotEmpty = function(n) {
        return !(h.isEmpty(n) && !h.isNumber(n) && !h.isBoolean(n));
    }, h.isElement = function(n) {
        return !(!n || 1 !== n.nodeType);
    }, h.isArray = c || function(n) {
        return "[object Array]" === o.call(n);
    }, h.isObject = function(r) {
        var t = n(r);
        return "function" === t || "object" === t && !!r;
    }, h.each([ "Arguments", "Function", "String", "Number", "Date", "RegExp", "Error" ], function(n) {
        h["is" + n] = function(r) {
            return o.call(r) === "[object " + n + "]";
        };
    }), h.isArguments(arguments) || (h.isArguments = function(n) {
        return h.has(n, "callee");
    }), "function" != typeof /./ && "object" != ("undefined" == typeof Int8Array ? "undefined" : n(Int8Array)) && (h.isFunction = function(n) {
        return "function" == typeof n || !1;
    }), h.isFinite = function(n) {
        return isFinite(n) && !isNaN(parseFloat(n));
    }, h.isNaN = function(n) {
        return h.isNumber(n) && n !== +n;
    }, h.isBoolean = function(n) {
        return !0 === n || !1 === n || "[object Boolean]" === o.call(n);
    }, h.isNull = function(n) {
        return null === n;
    }, h.isUndefined = function(n) {
        return void 0 === n;
    }, h.has = function(n, r) {
        return null != n && a.call(n, r);
    }, h.noConflict = function() {
        return root._ = previousUnderscore, this;
    }, h.identity = function(n) {
        return n;
    }, h.constant = function(n) {
        return function() {
            return n;
        };
    }, h.noop = function() {}, h.property = m, h.propertyOf = function(n) {
        return null == n ? function() {} : function(r) {
            return n[r];
        };
    }, h.matcher = h.matches = function(n) {
        return n = h.extendOwn({}, n), function(r) {
            return h.isMatch(r, n);
        };
    }, h.times = function(n, r, t) {
        var e = Array(Math.max(0, n));
        r = v(r, t, 1);
        for (var u = 0; u < n; u++) e[u] = r(u);
        return e;
    }, h.random = function(n, r) {
        return null == r && (r = n, n = 0), n + Math.floor(Math.random() * (r - n + 1));
    }, h.now = Date.now || function() {
        return new Date().getTime();
    };
    var N = {
        "&": "&amp;",
        "<": "&lt;",
        ">": "&gt;",
        '"': "&quot;",
        "'": "&#x27;",
        "`": "&#x60;"
    }, B = h.invert(N), q = function(n) {
        var r = function(r) {
            return n[r];
        }, t = "(?:" + h.keys(n).join("|") + ")", e = RegExp(t), u = RegExp(t, "g");
        return function(n) {
            return n = null == n ? "" : "" + n, e.test(n) ? n.replace(u, r) : n;
        };
    };
    h.escape = q(N), h.unescape = q(B), h.result = function(n, r, t) {
        var e = null == n ? void 0 : n[r];
        return void 0 === e && (e = t), h.isFunction(e) ? e.call(n) : e;
    };
    var T = 0;
    h.uniqueId = function(n) {
        var r = ++T + "";
        return n ? n + r : r;
    }, h.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var R = /(.)^/, K = {
        "'": "'",
        "\\": "\\",
        "\r": "r",
        "\n": "n",
        "\u2028": "u2028",
        "\u2029": "u2029"
    }, z = /\\|'|\r|\n|\u2028|\u2029/g, D = function(n) {
        return "\\" + K[n];
    };
    h.template = function(n, r, t) {
        !r && t && (r = t), r = h.defaults({}, r, h.templateSettings);
        var e = RegExp([ (r.escape || R).source, (r.interpolate || R).source, (r.evaluate || R).source ].join("|") + "|$", "g"), u = 0, i = "__p+='";
        n.replace(e, function(r, t, e, o, a) {
            return i += n.slice(u, a).replace(z, D), u = a + r.length, t ? i += "'+\n((__t=(" + t + "))==null?'':_.escape(__t))+\n'" : e ? i += "'+\n((__t=(" + e + "))==null?'':__t)+\n'" : o && (i += "';\n" + o + "\n__p+='"), 
            r;
        }), i += "';\n", r.variable || (i = "with(obj||{}){\n" + i + "}\n"), i = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + i + "return __p;\n";
        try {
            var o = new Function(r.variable || "obj", "_", i);
        } catch (n) {
            throw n.source = i, n;
        }
        var a = function(n) {
            return o.call(this, n, h);
        }, c = r.variable || "obj";
        return a.source = "function(" + c + "){\n" + i + "}", a;
    }, h.chain = function(n) {
        var r = h(n);
        return r._chain = !0, r;
    };
    var P = function(n, r) {
        return n._chain ? h(r).chain() : r;
    };
    h.mixin = function(n) {
        h.each(h.functions(n), function(r) {
            var t = h[r] = n[r];
            h.prototype[r] = function() {
                var n = [ this._wrapped ];
                return u.apply(n, arguments), P(this, t.apply(h, n));
            };
        });
    }, h.mixin(h), h.each([ "pop", "push", "reverse", "shift", "sort", "splice", "unshift" ], function(n) {
        var t = r[n];
        h.prototype[n] = function() {
            var r = this._wrapped;
            return t.apply(r, arguments), "shift" !== n && "splice" !== n || 0 !== r.length || delete r[0], 
            P(this, r);
        };
    }), h.each([ "concat", "join", "slice" ], function(n) {
        var t = r[n];
        h.prototype[n] = function() {
            return P(this, t.apply(this._wrapped, arguments));
        };
    }), h.prototype.value = function() {
        return this._wrapped;
    }, h.prototype.valueOf = h.prototype.toJSON = h.prototype.value, h.prototype.toString = function() {
        return "" + this._wrapped;
    }, "function" == typeof define && define.amd && define("underscore", [], function() {
        return h;
    });
}.call(void 0);