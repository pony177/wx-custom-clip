// index.ts
// 获取应用实例
const app = getApp<IAppOption>()

Page({
  data: {
    show: false,
    image: '',
    src: '',
    shadow: 0,
  },

  onLoad() {
    
  },

  async onChooseImage() {
    const { tempFiles: [{ tempFilePath: mPath }] } = await wx.chooseMedia({ mediaType: ['image'] })
    this.setData({
      src: mPath,
      show: true
    })
  },

  onCancel() {
    this.setData({ show: false })
  },

  onConfirm({ detail }: { detail: Record<string, any> }) {
    const mDot = detail.dot as Array<{ x: number, y: number }>
    wx.uploadFile({
      url: 'https://sf.gz.cn/product/clip/api/upload',
      filePath: this.data.src,
      name: 'file',
      timeout: 1000 * 500,
      formData: {
        pointer: JSON.stringify(mDot.map(item => [~~item.x, ~~item.y])), // 剪裁的四个坐标
        size: JSON.stringify([800, 1066]), // 剪裁后的图片大小
        shadow: this.data.shadow // 是否去除阴影 0 = 不去除; 1 = 去除
      },
      success: ({ data }) => {
        const mClip = this.selectComponent('#clip')
        const canvas = wx.createOffscreenCanvas({ type: '2d' })
        const ctx = canvas.getContext('2d')
        const fs = wx.getFileSystemManager()
        const mFilePath = `${wx.env.USER_DATA_PATH}/${new Date().getTime()}.jpg`
        fs.writeFileSync(mFilePath, data.slice(22), 'base64')
        const mTempImage = canvas.createImage()
        mTempImage.src = mFilePath
        mTempImage.onload = () => {
          canvas.width = mTempImage.width
          canvas.height = mTempImage.height
          mClip.drawRoundRectPath({
            ctx,
            width: mTempImage.width,
            height: mTempImage.height,
            radius: 20
          })
          ctx.drawImage(mTempImage, 0, 0, mTempImage.width, mTempImage.height)
          this.setData({
            show: false,
            image: ctx.canvas.toDataURL()
          })
        }
      },
      complete: () => wx.hideLoading()
    })
  },

  onImageTouch() {
    wx.previewImage({
      urls: [this.data.image]
    })
  },

  onCheck() {
    this.setData({
      shadow: this.data.shadow === 0 ? 1 : 0
    })
  }
})
